Subject: Release Update: architecture status, release goal status, BSPs
To: debian-devel-announce@lists.debian.org

Hi,

It's that time again where you get to hear the progress of our lovely
distribution, and what we're doing to get it out on the streets on time.

Architecture status
~~~~~~~~~~~~~~~~~~~

The armel architecture has shown promising development in the past few
weeks. If all continues well, we are optimistic that we can upgrade it
to a normal release architecture soon.  Please note that there now exists a
developer-accessible machine for porting efforts, agricola.debian.org.

Arm and hppa on the other hand are currently not really keeping up with
unstable anymore. This is mostly due to hardware issues (i.e. machines being
down) which we hope are resolved soon. However, Lenny will be the last release
for the architecture "arm" which will be superseded by armel if all goes to
plan.


Release goals
~~~~~~~~~~~~~
Regular work over the past months and the BSPs last weekend have helped
a lot with our release goals. Let's review the status again:

* Support for future gcc versions

gcc-4.3 is now the default on may archs, which makes the g++-4.3 FTBFSes
RC. Bugs severities have been raised earlier this week, which explains the
sudden RC bug count growth. We feel that those bugs have been around for
quite a lot of time, that's why we decided that packages affected by such
build problems will be the target of a more aggressive removal policy. We
will remove affected packages after the next weekend (that is, from April
14th on), so please fix your packages before that date.

* Switch /bin/sh to dash

With only about a hundred outstanding bugs, this goal just needs another
NMU campaign to come near to finishing. However, switching the default
installations won't happen anymore for Lenny, but we want to support all
users who want to use dash as default as good as possible.

* piuparts-clean archive

Over 50 bugs remaining, many with little activity.  Since these are
problems that affect all users and are usually fixed by little changes to
the maintainer scripts, more attention to this goal would be very
welcome.

* double compilation support

This goal still has over a hundred outstanding issues. Help would be
appreciated. Please note that many of the packages still affected are in
bad general shape, so each NMUer should consider if the package in
question shouldn't be removed instead.

* Prepare init.d-Scripts for dependency-based init systems

Almost all of these issues have been solved in the last few days. The few
remaining bugs should be closed in the next month.

* No unmet recommends relations inside main

This goal has been almost finished. New, yet unfiled issues, have been
added as bug reports, but there is also no a Wiki page to coordinate
the work. [RG:R]

* I18n support in all debconf-using packages

This goal has been almost finished.

* Support for python2.5

Almost finished. We will switch the default python version to 2.5 in the
next few days and upgrade the remaining issues to release critical bugs.

* Transition g77 -> gfortran

Finished, just needs a few more packages to transition to testing.


BSP Marathon
~~~~~~~~~~~~
At time of writing, we have 400 open RC bugs, which is 400 too many.  A
coordinated effort is needed to reduce this number, so we've decided to
resurrect last year's very successful BSP marathons. As a reminder, we
still have a 0-day NMU policy in effect.

Please note that in a BSP, you shouldn't just NMU every RC bug you see.
While you are working on a package, check for other low-hanging fruits
(like translation updates, typos that can easily be fixed, ...) and fix
them in your NMU. On the other hand, if you notice that a package looks
unmaintained, refrain from fixing the bugs for now and try to find out if
the package should be removed or adopted by another maintainer instead.

To give our BSPs a more targetted feeling, we want to assign one group of
RC bugs and one release goal to each weekend:

BSP on weekend 2008-05-01 to 2008-05-04
---------------------------------------
 + Fix remaining problems with dash as /bin/sh [RG:D]
 + Fix piuparts problems [RG:P]
 + Mass upgrade tests


Package team news
~~~~~~~~~~~~~~~~~

 * The glibc team sent a "Bits from the GNU Libc Maintainers" [GLIBC] with a
   detailed list of remaining issues, and the progress in their handling.
   Since that mail, the source of #442858 has been found, it's not really a
   bug but a gcc change that needs more flags to be passed to the compiler
   at link time, and that fact will be documented soon.

   We would like to see more mails like this one without us needing to ask for
   them, as it helps the Release Team planning the freeze.

 * KDE 4.1 prepared in experimental  
   The Qt/KDE team has decided to focus on getting KDE 4.1 in shape and thus
   stop efforts on KDE 4.0.x. This version is not released yet, but development
   snapshots from the KDE upstream subversion repository are available. These
   are partially available from experimental, but still need further work. A
   usable version ready for broader testing will hopefully be available in the
   next months. As Qt 4.4 is needed for the new KDE packages, its first
   release candidate will be uploaded to unstable really soon.
   On the KDE 3.5.9 front, no big changes are to reported. It has finally
   migrated to testing and it looks like there will be no 3.5.10 release.
 
 * GNOME 2.22 on its way to lenny
   The last GNOME release, 2.22, entails some big changes, migrating away
   from the well-known gnome-vfs to gvfs, a similar, but saner implementation
   of a virtual file system in glib. As these changes are not perfectly
   mature yet, some of the packages more affected by them, like the nautilus
   file manager, have only been uploaded to experimental. Most other packages
   have entered unstable and some already migrated to lenny. In the coming
   weeks, the first bug fix release, 2.22.1 is scheduled to be uploaded
   and move to testing.

 * Iceweasel/Firefox and other Mozilla stuff.
   A xulrunner 1.9b4 package has been recently uploaded in experimental
   after some unfortunate delays. Testing and porting work can now be
   started on reverse dependencies with the goal to have all these
   built against xulrunner 1.9 when lenny is released.

   A new upload for version 1.9b5 will happen shortly. Following this, a new
   iceweasel 3.0 beta (built on top of xulrunner) will be uploaded to
   experimental.

   While waiting for this to happen, an iceweasel 3.0 beta already
   available in experimental for users to test. It has a known issue
   that icons aren't displayed unless you install iceweasel-gnome-support.

   The Team's plans with respect to iceape and icedove are unclear at the
   moment as it's unsure which new upstream releases will happen in time
   for lenny.



Essential Packages frozen
~~~~~~~~~~~~~~~~~~~~~~~~~
For reading this far, you receive the small reward of the knowledge that
packages marked as essential will not automaticaly migrate to testing any
more.  Read as: the freeze process for the upcoming release of Debian Lenny
started.  If you need your package unblocked, please contact the release
team on its canonical address: debian-release@lists.debian.org.



Default syslog
~~~~~~~~~~~~~~
We are currently discussing to change the default syslog daemon to rsyslog.
There is no final decision yet, but things look rather optimistic. If you
want, you can of course already try out rsyslog on your system.



Tricks from the Release Team
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you want to know the wanna-build status of your package, [WB] has some nice
informations for you, like which package builds on which buildd, and who much
the "needs-build" queue is filed up for an architecture. You can also just
query your package.

Cheers,
NN
-- 
http://release.debian.org
Debian Release Team

References:
 [RG:D] http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-dash
 [RG:P] http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=piuparts-stable-upgrade
 [RG:R] http://wiki.debian.org/ReleaseGoalRecommends
 [GLIBC] Message-Id: <20080330192859.GA12760@hall.aurel32.net>
         http://lists.debian.org/debian-release/2008/03/msg00367.html
 [WB] http://buildd.debian.org/~jeroen/status/
