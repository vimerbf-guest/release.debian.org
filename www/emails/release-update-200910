To: debian-devel-announce@lists.debian.org
Subject: Bits from the release team: Planning, request for help

Hi

Release planning
================
Proposing a new freeze date is not easy. Taking into account all of
the feedback we have received, both online (by e-mail, IRC) as well as 
in person, and some challenging release goals we have set for ourselves,
we propose freezing in March 2010.

Request for help
================
As in our previous update, we want to stress that the state
of unstable and testing is not very good. We still have a large number
of Release Critical bugs and problems ensuring the smooth
transition of packages to testing.

To get the number of RC bugs down, we will be more aggressively
removing fringe packages from testing and request your help in
fixing bugs, following the build status of your packages and
organising bug squashing parties (BSPs [0][1][2]).

In order to help ensure the smooth migration to testing of packages 
involved in library transitions, we ask you to try to avoid uploading 
packages when doing so would disturb an existing transition. Executing 
``grep-excuses <pkg>'' (from the devscripts package) before uploading 
can help a lot.

If you are interested in helping or joining the Release Team, please
contact us (in private or public) and we'll see how to make it work.

Please don't hesitate to contact us if you have any questions.

-- 
http://release.debian.org
Debian Release Team

[0] http://wiki.debian.org/BSP
[1] http://wiki.debian.org/BSP2009/CambridgeNovember
[2] http://wiki.debian.org/BSP2010/Moenchengladbach
