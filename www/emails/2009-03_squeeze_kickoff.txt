To: debian-devel-announce@lists.debian.org
Subject: Kicking off Squeeze: initial tidbits from the RMs

Hello!

About a month has passed since we released Lenny, and it’s time to
formally kick off Squeeze development. We’ve been busy with other stuff,
but finally we’ve sat down and prepared an initial “big picture” mail.
This first mail (in a hopefully regular series) is a declaration of
principles, a call for feedback and ideas, a planning of our next
actions, an update on the current state of affairs, and some other bits.
Please read on!


Principles
==========

I believe it’s very important we communicate these clearly. It’s going
to sound like a joke, but the Release Managers will take them very
seriously. Let’s start with a statement of purpose:

    The main aim of the Release Team is to help the Project deliver a
    release the developers will be proud of, with a development process
    they’ll find satisfactory, and with a balanced timeline that will
    meet the needs of developers and end-users.

Wow, that’s ambitious, but we truly believe in it. I don’t like the
vision (not that it’s been common) that Release Managers are here to
dictate what the Project should do. Instead, I believe it’s of critical
importance that the RMs place their efforts in ensuring the concerns and
desires of the developers get addressed and materialized to the possible
extent.

So, here’s our deal for the Squeeze development cycle, during which we
will:

    * actively seek and act upon feedback and criticism from the
      developers, in order to prevent past mistakes from happening
      again, and improve the interaction between the RT and the rest of
      the project.

    * engage in discussions with the developers at large, as well as
      particular groups and teams, to get a clear picture of what their
      concerns, objectives and proposed solutions are.

    * make judgement calls doing our best to stay in the same page as
      the rest of the developers, clearly communicating our rationales
      so they can be discussed.

    * be permanently open to ongoing feedback and criticism. In fact, we
      will depend on it to live up to our standards, because forgetting
      about them is easy in stressful times.

    * cut people lots of slack, because we’re only all human and we all
      deserve it.

And enough said already, hopefully for a long time. Kicking off proceeds.


Criticism and wishes
====================

                ***  feedback@release.debian.org  ***

It’s rant-time o’clock. We would like you to stop and think about:

    * things you didn’t like, or that plainly sucked, about the Lenny
      development cycle or about Lenny itself, why they were a problem,
      and (if you can) your proposed solutions. We are aware of many of
      them already, but mentioning them will allow us to see which of
      them are the biggest worries among the developers, and everybody
      will have different ideas on how to solve them.

    * things you think can be improved in our development cycles, or
      that could be done differently for Squeeze, why they are important
      to address, and how we could do better in those regards.

We ask that you mail us in private, to the above address. We would
prefer not to start on-list discussions yet if that’ll be okay with you,
because we believe it’s going to be better if we process all the
feedback at once, detect common concerns, and do an orderly discussion
afterwards; see below about this.

The above address is also the address to use during all of Squeeze’s
development cycle when you have to call on us about ongoing problems or
concerns. Emails to that address are not archived, and received only by
the Release Managers, though of course unless explicitly noted, the
ideas expressed in them will be mentioned in public when it’s necessary
to have discussions about them.


Next actions
============

The Release Team hopes to hold a face-to-face meeting during April. Our
intent is to process and dissect all the feedback and ideas received,
and to make a plan out of it. Or, rather, a proposal for a plan. We will
detect common areas of concern, and we’ll present to the project our
proposed solutions where they’re straightforward to address, and our
ideas to solve them where not, in order to discuss them project-wide
during May and June.

In the meantime, we don’t want the need for these discussions to block
any development to happen, or prevent work on goals for Squeeze from
starting. Technical discussions can happen at any time, so please do
start to bring up for review and discussion the project-wide improvements
that you’d like to see happen in Squeeze. 

We’ll be keeping an eye on several lists, but if you want to make sure
you grab our attention, or you’re actually proposing a Release Goal,
please Bcc debian-release@lists.debian.org. Discussion should not happen
there, though: in general, debian-devel is the appropriate list for
project-wide development matters, and other developers should get a
chance to raise their concerns about a proposed Release Goal.

You can make Squeeze rock if you’re committed to it!


Immediate affairs
=================

During this past month, the Release Team has been busy with approving
and tracking library transitions. We’ve recently sent an update to the
debian-devel mailing list [1] about some progress in future handling of
these.

  [1]: http://lists.debian.org/debian-devel/2009/03/msg00775.html

As a reminder, and until we have proper documentation of procedures in
this area in place, just make sure:

    * you coordinate library transitions with debian-release@lists.d.o

    * you provide information about whether reverse dependencies rebuild
      fine without changes, or source adjustments are needed

    * do not gratuitously rename your development packages without
      assessment from us, as to facilitate automatic rebuilds of reverse
      dependencies


And that’s all for now.

Cheers,
